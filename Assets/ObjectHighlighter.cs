﻿using UnityEngine;
using System.Collections;

public class ObjectHighlighter : MonoBehaviour {
	bool isHighlighted = false;
	Material originalMaterial;
	Material redMaterial;
	Texture placeHolder;

	MeshRenderer meshRenderer;
	
	GameObject baseObject;
	string obj_name;
	int current = 0;

	private string url = "http://organicthemes.com/demo/profile/files/2012/12/profile_img.png";
	private GameObject profileImage;

	private GameObject childUI;
	private GameObject cameraOVR;
	private GameObject respawn;
	public Transform[] chekpoints;
	public float nextRadius;
	public float speed;
	public Transform target;

	// Use this for initialization
	void Start () {
		
		obj_name        = this.gameObject.name;
		baseObject      = GameObject.Find( obj_name );
		meshRenderer        = baseObject.GetComponent<MeshRenderer>();
		originalMaterial    = meshRenderer.material;

		childUI = 	GameObject.Find("UI").gameObject;
		profileImage = childUI.transform.FindChild("ProfileImg").gameObject;
		profileImage.SetActive(false);
		cameraOVR = GameObject.Find ("UIAnchor");
		respawn = GameObject.Find ("Checkpoint1");

		redMaterial = Resources.Load("VolHighlight", typeof(Material)) as Material;
		placeHolder = Resources.Load("person", typeof(Texture)) as Texture;
		childUI.SetActive(false);
	}

	IEnumerator LoadProfPic(){
		WWW www = new WWW (url);
		yield return www;
		if (string.IsNullOrEmpty(www.error)){
			if (www.texture != null) {
				Debug.Log ("Loaded this url: " + url);
				profileImage.GetComponent<UnityEngine.UI.RawImage> ().texture = www.texture;
			}
		} else {
			Debug.Log("Failed to load this url: " + url);
			profileImage.GetComponent<UnityEngine.UI.RawImage> ().texture = placeHolder;
		}
		profileImage.SetActive (true);
	}

	// Update is called once per frame
	void Update () {
		Vector3 dir = target.position - transform.position;
		float dist = dir.magnitude;
		dir = dir.normalized;
		float move = speed * Time.deltaTime;
		if(move > dist) move = dist;
		transform.Translate( dir * move);

		if (dir == Vector3.zero) {
			transform.position = respawn.transform.position;
		}

		if ((transform.position - target.position).magnitude < nextRadius) {
			current++;
			if(current == chekpoints.Length) 
				current = 0;
			target.position = chekpoints[current].position;
		}
	}
	
	void OnMouseUp(){
		
		Debug.Log("OMD "+obj_name);
		isHighlighted = !isHighlighted;
		
		if( isHighlighted == true ){
			StartCoroutine("LoadProfPic");

			Vector3 position = new Vector3(cameraOVR.transform.position.x, cameraOVR.transform.position.y, cameraOVR.transform.position.z);
			childUI.transform.position = position;
			childUI.transform.LookAt(childUI.transform.position + cameraOVR.transform.rotation * Vector3.forward, cameraOVR.transform.rotation * Vector3.up);
			childUI.SetActive(true);
		}
	}

	void OnMouseEnter(){
		HighlightRed();
	}

	void OnMouseExit(){
		RemoveHighlight();
	}
	
	void HighlightRed(){
		meshRenderer.material = redMaterial;
		Debug.Log("IT SHOULD BE RED");
	}
	
	void RemoveHighlight(){
		meshRenderer.material = originalMaterial;
	}
}