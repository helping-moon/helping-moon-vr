﻿using UnityEngine;
using System.Collections;

public class TestEvents : MonoBehaviour {

	private AndroidJavaClass activityClass;
	private AndroidJavaClass mainActivity;
	private AndroidJavaObject activity;

	private AndroidJavaClass androidClass;
	private AndroidJavaClass unityActivity;
	private AndroidJavaObject androidObj;
	private GameObject volUI;

	private bool _testBool;
	public bool testBool {
		get {
			return _testBool;
		}
		set {
			_testBool = value;
			Debug.Log ("Test Bool set to "+value);
		}
	}

	private float _testFloat;
	public float testFloat {
		get {
			return _testFloat;
		} 
		set {
			_testFloat = value;
			Debug.Log ("Test Float set to "+value);
		}
	}

	public void TestButton() {
		Debug.Log ("Test Button pressed");
		Handheld.PlayFullScreenMovie ("video.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
		//nativeCall ();
	}

	public void CloseVolunteerUI(){
		volUI = GameObject.Find ("UI");
		volUI.SetActive (false);
	}

	public void ObjectSelect(){
		Handheld.PlayFullScreenMovie ("video.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
		androidClass = new AndroidJavaClass ("com.Oculus.UnitySample.TestActivity");
		unityActivity = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		androidObj = unityActivity.GetStatic<AndroidJavaObject>("currentActivity");
		androidObj.Call ("Lunch");
	}

	private void nativeCall(){
		activityClass = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		activity = activityClass.GetStatic<AndroidJavaObject> ("currentActivity");
		mainActivity = new AndroidJavaClass ("com.oculus.oculus360videossdk.MainActivity");
		mainActivity.CallStatic ("nativeCall", activity);
	}

}
