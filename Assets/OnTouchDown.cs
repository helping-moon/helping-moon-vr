﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class OnTouchDown : MonoBehaviour {

	// Use this for initialization
	private GameObject cameraObj;
	private Camera cameraRef;
	private MeshRenderer meshRenderer;
	private PointerEventData lookData;

	void Start () {
		cameraObj = GameObject.Find ("Look Camera");
		cameraRef = cameraObj.GetComponent<Camera> ();
		meshRenderer.material = new Material (Shader.Find ("Transparent/Parallax Specular"));
		meshRenderer.material.color = Color.white;

	}

	// Update is called once per frame
	void Update () {
		int touchCorrection = 1;
		
		RaycastHit hit = new RaycastHit();
		for (int i = 0; i+touchCorrection < Input.touchCount; ++i) {
			if (Input.GetTouch(i).phase.Equals(TouchPhase.Began)) {
				// Construct a ray from the current touch coordinates
				Ray ray = cameraRef.ScreenPointToRay(new Vector3(200, 200, 0));
				Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
				if (Physics.Raycast(ray, out hit)) {
					hit.transform.gameObject.SendMessage("OnMouseDown");
			}
		}
	}

		Ray raycast = new Ray (cameraRef.transform.position, cameraRef.transform.forward);
		if (Physics.Raycast(raycast, out hit)){
			if (hit.transform.gameObject.tag.Equals("Volunteer")){
				GameObject vol = hit.transform.gameObject;
				MeshRenderer mr = vol.GetComponent<MeshRenderer>();
				mr = meshRenderer;
			}
		}

	}
}
