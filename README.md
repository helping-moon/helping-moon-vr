# Helping Moon VR
> Unity3D VR Project for helping moon

## Prerequisites:
* [Unity 5.1.1p3 Patched](http://unity3d.com/unity/qa/patch-releases/5.1.1p3)
* [Oculus Mobile SDK with Unity Integration 0.6.0.1](https://developer.oculus.com/downloads/mobile/0.6.0.1/Oculus_Mobile_SDK/)
* From Oculus SDK, import `VrSamples\Unity\UnityIntegration\UnityIntegration.unityPackage` into Unity project
* Use provided ProjectSettings

* Oulu Meshes need to be downloaded separately into `Assets/Meshes`